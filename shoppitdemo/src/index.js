import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './Layout.js';
import UserPanel from './UserPanel.js';
import Navigation from './Navigation.js';
import FilterBar from './FilterBar.js';
import Posts from './Posts.js';
import App from './App.js';
// import newPostModal from "./components/Modal.js";




//
ReactDOM.render(
  <Layout/>,
  document.getElementById('root')
);

ReactDOM.render(
  <Navigation/>,
  document.getElementById('nav-area')
);

ReactDOM.render(
  <UserPanel/>,
  document.getElementById('user-panel')
);

ReactDOM.render(
  <FilterBar/>,
  document.getElementById('filter-area')
);

ReactDOM.render(
  <Posts/>,
  document.getElementById('post-area')
);
//
// ReactDOM.render(
//   <App/>,
//   document.getElementById('user-panel')
// );