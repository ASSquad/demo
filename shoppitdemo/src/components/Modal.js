import React from "react";
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'
import Form from 'react-bootstrap/Form'

class NewPostModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        activeItem: this.props.activeItem
    };
  }

  // static getDerivedStateFromProps(props, state) {
  //   return {activeItem: props.activeItem };
  // };

  handleChange = e => {
    let { name, value } = e.target;
    if (e.target.type === "checkbox") {
      value = e.target.checked;
    }
    if (e.target.type === "file") {
      value = e.target.files[0];
    }
    const activeItem = { ...this.state.activeItem, [name]: value };
    this.setState({ activeItem });

  };


  creatFormData(){

    let form_data = new FormData();
    form_data.append('image', this.state.activeItem.image, this.state.activeItem.image.name);
    form_data.append('title', this.state.activeItem.title);
    form_data.append('description', this.state.activeItem.description);
    form_data.append('author', "guest");
    form_data.append('tags', ["product"]);
    form_data.append('ratings', [0]);
    form_data.append('date', Date());
    return form_data;
  };

  render() {
    const { toggle, onSave } = this.props;
    return (
      <Modal.Dialog isOpen={true} show={toggle}>
        <Modal.Header> Create New Post </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                name="title"
                value={this.state.activeItem.title}
                // inputRef={node => this.state.activeItem.title = node}
                onChange={this.handleChange}
                placeholder="Enter Title"
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                name="description"
                value={this.state.activeItem.description}
                // inputRef={node => this.state.activeItem.description = node}
                onChange={this.handleChange}
                placeholder="Enter description"
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Image</Form.Label>
                <Form.Control
                  id="image"
                  type="file"
                  name="image"
                  accept="image/png ,image/jpeg,image/jpg"
                  // value={this.state.activeItem.image}
                  // inputRef={node => this.state.activeItem.image = node}
                  onChange={this.handleChange}
                  //placeholder="Enter Image URL"
                />

            </Form.Group>
            <Form.Group>
                <Form.Control
                  id="author"
                  type="hidden"
                  name="author"
                  value={"guest"}
                />
                <Form.Control
                  id="ratings"
                  type="hidden"
                  name="ratings"
                  value={0}
                />
                <Form.Control
                  id="tags"
                  type="hidden"
                  name="tags"
                  value={'product'}
                />
                <Form.Control
                  id="date"
                  type="hidden"
                  name="date"
                  value={Date()}
                />

            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {/*<Button color="success" onClick={() => onSave(this.state.activeItem)}>*/}
          <Button color="success" onClick={() => onSave(this.creatFormData())}>
            Save
          </Button>
        </Modal.Footer>
      </Modal.Dialog>
    );
  }
}

export default NewPostModal;