import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button'
import Image from 'react-bootstrap/Image'
import Posts from "../Posts";
import ReactDOM from "react-dom";

// var username='';



class Nav extends React.Component {

    constructor(props) {
    super(props);
    this.state = {
          logged_in: localStorage.getItem('token') ? true : false,
          display_form:'',
          handle_logout:'',
          username:''
    };
  }


  componentDidMount() {



  }

    static getDerivedStateFromProps(props, state) {


        return {display_form:props.display_form,handle_logout:props.handle_logout };
    }

    render(){

        if (localStorage.getItem('token')) {
          fetch('http://shehzikhan.ml:8000/current_user/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
            .then(res => res.json())
            .then(json => {
                this.setState({ logged_in: true });
              this.setState({ username: json.username });
            });
        }

      const logged_out_nav = (

        <div>
          <Button onClick={() => this.state.display_form('login')} variant="primary" size="lg" block>
            Login
          </Button>
          <Button onClick={() => this.state.display_form('signup')}variant="secondary" size="lg" block>
            Sign Up
          </Button>
        </div>
      );

      const logged_in_nav = (

        <div >
          <Image src="images/avatar.png" height="150px" style={{positiom:"relative"}} roundedCircle />
            <div className="bg-light" style={{positiom:"absolute",transform: "translate(0%, -99%)",display:"inline-block", background:"white",height:"50px",width:"150px",borderRadius: "0 0 50% 50%",opacity:"0.8"}}>
                <strong>{this.state.username}</strong>
                <p onClick={this.state.handle_logout}>Logout</p>
            </div>
        </div>

      );
      return <div>{(localStorage.getItem('token')) ? logged_in_nav : logged_out_nav}</div>;
    }
}

export default Nav;

Nav.propTypes = {
  logged_in: PropTypes.bool.isRequired,
  display_form: PropTypes.func.isRequired,
  handle_logout: PropTypes.func.isRequired
};