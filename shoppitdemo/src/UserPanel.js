import React from 'react';
// import ReactDOM from 'react-dom';
// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'
// import NavDropdown from 'react-bootstrap/NavDropdown'
// import Form from 'react-bootstrap/Form'
// import FormControl from 'react-bootstrap/FormControl'
// import Button from 'react-bootstrap/Button'

// import Col from 'react-bootstrap/Col'
// import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'
import Image from 'react-bootstrap/Image'
import LoginForm from "./components/LoginForm";
import SignupForm from "./components/SignupForm";
import Nav from "./components/Nav";
import Posts from './Posts.js';
import ReactDOM from "react-dom";

// import Figure from 'react-bootstrap/Figure'


class UserPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayed_form: '',
      logged_in: localStorage.getItem('token') ? true : false,
      username: ''
    };
  }

  componentDidMount() {
    if (this.state.logged_in) {
      fetch('http://shehzikhan.ml:8000/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(json => {
          this.setState({ username: json.username });
        });
    }
  }

  handle_login = (e, data) => {
    e.preventDefault();
    fetch('http://shehzikhan.ml:8000/token-auth/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(json => {
        localStorage.setItem('token', json.token);

        this.setState({
          logged_in: true,
          displayed_form: '',
          username: json.user.username
        });

        ReactDOM.render(<Posts logged_in={true}/>, document.getElementById('post-area'));

      });


  };

  handle_signup = (e, data) => {
    e.preventDefault();
    fetch('http://shehzikhan.ml:8000/users/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(json => {
        localStorage.setItem('token', json.token);
        this.setState({
          logged_in: true,
          displayed_form: '',
          username: json.username
        });
        ReactDOM.render(<Posts logged_in={true}/>, document.getElementById('post-area'));

      });
  };

  handle_logout = () => {
    localStorage.removeItem('token');
    this.setState({ logged_in: false, username: '' });
    ReactDOM.render(<Posts logged_in={false}/>, document.getElementById('post-area'));


  };

  display_form = form => {
    this.setState({
      displayed_form: form
    });
  };

  render() {

    let form;
    switch (this.state.displayed_form) {
      case 'login':
        form = <LoginForm handle_login={this.handle_login} />;
        break;
      case 'signup':
        form = <SignupForm handle_signup={this.handle_signup} />;
        break;
      default:
        form = null;
    }

    // const logged_out_nav = (
    //     <ul>
    //       <li onClick={() => this.display_form('login')}>login</li>
    //       <li onClick={() => this.display_form('signup')}>signup</li>
    //     </ul>
    //   );
    //
    // const logged_in_nav = (
    //     <ul>
    //       <li onClick={this.handle_logout}>logout</li>
    //     </ul>
    // );


    return (
        <Container className="text-center">
          <div>
              <Image src="images/logo.jpg" />

          </div>
            <br />
            <br />
          {/*<div >*/}
              {/*<Image src="images/avatar.png" height="150px" style={{positiom:"relative"}} roundedCircle />*/}
                {/*<div className="bg-light" style={{positiom:"absolute",transform: "translate(0%, -99%)",display:"inline-block", background:"white",height:"50px",width:"150px",borderRadius: "0 0 50% 50%",opacity:"0.8"}}>*/}
                    {/*<strong>UserName</strong>*/}
                    {/*<p>Logout</p>*/}
                {/*</div>*/}
          {/*</div>*/}
          <div className="App">
            <Nav
              logged_in={this.state.logged_in}
              display_form={this.display_form}
              handle_logout={this.handle_logout}
            />
            {/*<div>{this.logged_in ? logged_in_nav : logged_out_nav}</div>*/}
            {form}

          </div>
        </Container>
    );
  }
}

export default UserPanel;