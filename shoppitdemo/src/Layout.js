import React from 'react';
// import ReactDOM from 'react-dom';
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
// import Container from 'react-bootstrap/Container'

class Layout extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div>
            <Row>
                <Col id="user-panel" className="bg-light" xs lg="2">

                </Col>
                <Col xs lg="10">


                        <div  id="nav-area"></div>
                        <div className="bg-light" id="filter-area" ></div>


                    <br />
                    <Row>

                            <Col id="post-area"></Col>
                            <Col id="details-area" xs lg="3">
                                <div className="text-center">

                                        <div id="relatedCat" className="card">
                                            <div className="card-header">Related Categories</div>
                                            <div className="card-body">Content</div>
                                        </div>
                                        <br />

                                        <div  className="card">
                                            <div className="card-header">Top Contributers</div>
                                            <div className="card-body">Content</div>
                                        </div>
                                        <br />

                                        <div  className="card">
                                            <div className="card-header">Top Products</div>
                                            <div className="card-body">Content</div>
                                        </div>
                                        <br />

                                        <div  className="card">
                                            <div className="card-header">Images</div>
                                            <div className="card-body">Content</div>
                                        </div>
                                        <br />
                                </div>
                            </Col>

                    </Row>
                </Col>
            </Row>
        </div>
    );
  }
}

export default Layout;