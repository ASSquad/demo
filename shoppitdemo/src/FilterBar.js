import React from 'react';
// import ReactDOM from 'react-dom';

import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Posts from './Posts.js';
import ReactDOM from "react-dom";
// import Container from 'react-bootstrap/Container'

class FilterBar extends React.Component {
  constructor(props) {
    super(props);
        this.state = {

          board:false,
    };
  }


  changeView(view){
      this.setState({board: view});
      ReactDOM.render(<Posts board={view} logged_in={localStorage.getItem('token') ? true : false}/>, document.getElementById('post-area'));
  }


  render() {
    return (
     //   <Container>
        <Row >
            <Col>
                    <Form inline>
                        <Form.Group>
                            <Form.Label inline style={{margin:"10px"}}>View: </Form.Label>

                            <a onClick={() => this.changeView(false)} href="#">
                                <span className="fas fa-list" style={{margin:"10px",color: "Mediumslateblue"}}></span>
                            </a>
                            <a onClick={() => this.changeView(true)}  href="#">
                                <span className="fas fa-th-large" style={{margin:"10px",color: "Mediumslateblue"}}></span>
                            </a>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label inline style={{margin:"10px"}}>Sort By: </Form.Label>
                            <Form.Control inline as="select">
                                <option>Relevance</option>
                                <option>Date</option>
                                <option>Rating</option>
                            </Form.Control>
                        </Form.Group>
                    </Form>
            </Col>
            <Col xs lg="2" >
                <Form >
                  <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                </Form>
            </Col>
        </Row>
     //   </Container>
    );
  }
}

export default FilterBar;