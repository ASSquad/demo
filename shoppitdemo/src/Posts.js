import React from 'react';


// import Col from 'react-bootstrap/Col'
// import Row from 'react-bootstrap/Row'
// import Container from 'react-bootstrap/Container'
import Media from 'react-bootstrap/Media'
import Alert from 'react-bootstrap/Alert'
import NewPostModal from "./components/Modal";
import axios from "axios";
import "./rating.css"

// import Figure from 'react-bootstrap/Figure'



class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
          modal: false,
          board:false,
          logged_in: localStorage.getItem('token') ? true : false,
          activeItem: {
            title: "",
            author:"",
            description: "",
            image:"",
            tags:[],
            ratings:[],
              date:''
          },
          postList: []
    };
  }

  static getDerivedStateFromProps(props, state) {
    return {board: props.board,logged_in: props.logged_in};
  }

  componentDidMount() {
        this.refreshList();
        // alert(this.state.logged_in)
      }
  refreshList = () => {
    axios
      .get("http://shehzikhan.ml:8000/api/posts/")
      .then(res => this.setState({ postList: res.data }))
      .catch(err => console.log(err));
  };
  toggle = () => {
        this.setState({ modal: !this.state.modal });
      };

  handleSubmit = item => {
    this.toggle();
    //alert("save" + JSON.stringify(item));
    if (item.id) {
          axios
            .put(`http://shehzikhan.ml:8000/api/posts/${item.id}/`, item)
                // , {
                //   headers: {
                //     'content-type': 'multipart/form-data'
                //   }})
            .then(res => this.refreshList());
          return;
        }
    axios
      .post("http://shehzikhan.ml:8000/api/posts/", item,
          {
          headers: {
           'content-type': 'multipart/form-data'
          }}
          )
      .then(res => this.refreshList());
  };

  handleDelete = item => {
        axios
          .delete(`http://shehzikhan.ml:8000/api/posts/${item.id}`)
          .then(res => this.refreshList());
      };

  createItem = () => {
        const item = { title: "",
            author:"",
            description: "",
            image:"",
            tags:[],
            rating:"",
        date:""};
        this.setState({ activeItem: item, modal: !this.state.modal });
      };

  editItem = item => {
        this.setState({ activeItem: item, modal: !this.state.modal });
      };


  ratingStars = (rating) => {

      let form = <form className="rating">
          <label>
              <input type="radio" name="stars" value="1"/>
              <span className="icon">★</span>
          </label>
          <label>
              <input type="radio" name="stars" value="2"/>
              <span className="icon">★</span>
              <span className="icon">★</span>
          </label>
          <label>
              <input type="radio" name="stars" value="3"/>
              <span className="icon">★</span>
              <span className="icon">★</span>
              <span className="icon">★</span>
          </label>
          <label>
              <input type="radio" name="stars" value="4"/>
              <span className="icon">★</span>
              <span className="icon">★</span>
              <span className="icon">★</span>
              <span className="icon">★</span>
          </label>
          <label>
              <input type="radio" name="stars" value="5"/>
              <span className="icon">★</span>
              <span className="icon">★</span>
              <span className="icon">★</span>
              <span className="icon">★</span>
              <span className="icon">★</span>
          </label>
      </form>



      return form
  };
  renderListView = () => {
    const newItems = this.state.postList;

    return newItems.map(item => (
          <div className="card" style={{ marginBottom:"10px",marginTop:"10px"}}>
            <div className="card-body">
                <Media>
                  <img
                    width={64}
                    height={64}
                    className="mr-3"
                    src={item.image}
                    alt="Generic placeholder"
                  />
                  <Media.Body>
                    <h5>{item.title}</h5>
                      <p>{item.author}</p>
                    <p>
                        {item.description}
                    </p>
                      <div>
                          Ratings: {this.ratingStars(item.ratings)} ({item.ratings.length}) <span style={{marginLeft:"20px"}}></span>  Tags: {item.tags.map((number) => <span style={{marginLeft:"10px"}} className="badge badge-primary badge-pill">{number}</span>)}
                      </div>
                  </Media.Body>
                </Media>
            </div>
          </div>

    ));
  };

  renderBoardView = () => {
    const newItems = this.state.postList;

    let cards= newItems.map(item => (
          <div className="card" style={{width:"300px",margin:"10px"}}>
              <img  className="card-img-top"
                    src={"../"+item.image}
                    alt="Generic placeholder"
                  />
            <div className="card-body">
                    <h5 className="card-title">{item.title}</h5>
                      <p className="card-text">{item.author}</p>
                    <p>
                        {item.description}
                    </p>
                      <div>
                          Ratings: {this.ratingStars(item.ratings)} ({item.ratings.length}) </div><div>  Tags: {item.tags.map((number) => <span style={{marginLeft:"10px"}} className="badge badge-primary badge-pill">{number}</span>)}
                      </div>
            </div>
          </div>


    ));
    return <div className="card-columns">{cards}</div>
  };



  render() {
    return (
        <div>
            {this.state.modal ? (
              <NewPostModal
                activeItem={this.state.activeItem}
                toggle={this.toggle}
                onSave={this.handleSubmit}
              />
            ):null}

            {localStorage.getItem('token') ? (
                <div className="">
                    <button onClick={this.createItem} className="btn btn-primary">
                      New Post
                    </button>
                </div>
            ):null}

            {this.state.board ? this.renderBoardView() : this.renderListView()}


        </div>

    );
  }
}

export default Posts;



