# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Post

class ShoppitAdmin(admin.ModelAdmin):
  list_display = ('title', 'description', 'image')

# Register models here.
admin.site.register(Post, ShoppitAdmin)
