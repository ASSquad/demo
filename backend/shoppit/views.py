# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, FormParser
from .serializers import PostSerializer
from .models import Post
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView



class PostView(APIView):
  parser_classes = (MultiPartParser, FormParser)
  # serializer_class = PostSerializer
  # queryset = Post.objects.all()

  def get(self, request, *args, **kwargs):
    posts = Post.objects.all()
    serializer = PostSerializer(posts, many=True)
    return Response(serializer.data)


def post(self, request, *args, **kwargs):
  posts_serializer = PostSerializer(data=request.data)
  if posts_serializer.is_valid():
    posts_serializer.save()
    return Response(posts_serializer.data, status=status.HTTP_201_CREATED)
  else:
    print('error', posts_serializer.errors)
    return Response(posts_serializer.errors, status=status.HTTP_400_BAD_REQUEST)