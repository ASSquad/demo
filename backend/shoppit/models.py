# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.postgres.fields import ArrayField


class Post(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    image = models.ImageField(upload_to='post_images')
    author = models.TextField()
    tags = ArrayField(models.TextField())
    ratings=ArrayField(models.IntegerField())
    date = models.DateTimeField()

    def _str_(self):
        return self.title