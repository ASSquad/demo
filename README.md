# demo

- This is a small demo project.

	- Clone the repo using git clone git@bitbucket.org:ASSquad/demo.git
	- cd demo

- Setup Database:
    - sudo apt install postgresql postgresql-contrib
    - sudo -i -u postgres
        - psql
        - postgres=# \password
        - \q
        - exit
        
    - sudo -u postgres createdb shoppitdb
    - pg_restore --host "localhost" --port "5432" --username "postgres" --password --dbname "shoppitdb" --verbose "/root/demo/db_backup/shoppit.sql"
   
    
- Setup BackEnd:
    - sudo apt install python3-pip
	- pip3 install pipenv
	- pipenv shell
	- pipenv install django
	- pipenv install djangorestframework
    - pipenv install djangorestframework-jwt
    - pipenv install django-cors-headers
    - pipenv install psycopg2-binary
	- django-admin startproject backend
	- cd backend
	- python3 manage.py startapp shoppit
	- python3 manage.py migrate
	- python3 manage.py runserver

- Setup FrontEnd:
	- curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
	- sudo apt-get install -y nodejs
	- npm install jquery
	- npm install react react-dom
	- npm install core-js
	- npm install react-scripts  (If failed due to vulnerabilities then fix then using npm audit fix --force and try again)
	- npm install react-bootstrap bootstrap
	- npm install axios
	- npm i prop-types -S
	- npm start